#!/usr/bin/python3

import os
import getpass

def clear():
    os.system("clear")


user = getpass.getuser()

print (f" Hello,{user.capitalize()}\n Welcome to your Music-Player")
dir = input("\n[!] Wich Directory Do You Want ?\n\n")

if dir.count("music") == 1 or dir.count("Music") == 1 :
    dir = "/home/"+user+"/Music"

clear()
os.chdir(dir)

files = [file for file in filter(os.path.isfile,os.listdir("."))]
print(" [S] Choose your target :\n")

for file in files:
    music = file
    if music.count("-") or music.count("(") or music.count(")"):
        music = music.replace("-","_").replace("(","[").replace(")","]")
        os.rename(file,music)
        print(f" [{files.index(file)+1}] {music} ")
        file = music
    else:
        print(f" [{files.index(file)+1}] {music} ")

files = [file for file in filter(os.path.isfile,os.listdir("."))]
choice = int(input("\n :"))
music = files[choice-1]
music = music.replace(" ","\ ")
clear()

times = input(""" [-] How Many Times Do You Want Play it
 [1] forever 'f'
 [2] one time 'o'
 [3] N times
 [*] For N times enter the N\n:""")

if times.count("f") == 1 or times.count("F") == 1 :
    cmd = "mpv --no-audio-display --loop inf " + music
    clear()
    os.system(cmd)
elif times.count("o") == 1 or times.count("O") == 1 :
    cmd = "mpv --no-audio-display  " + music
    os.system("clear")
    os.system(cmd)
else :
    cmd = "mpv --no-audio-display --loop " + times + " " + music
    os.system("clear")
    os.system(cmd)
